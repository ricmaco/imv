#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Packages needed:
# - mpv
# - pyqt4

'''
Execute some test videos in order to evaluate codec quality.
Interface done with PyQt and video displayed with mpv video player.
'''

import subprocess as sp
import sys, os
from os import path
from datetime import datetime as dt

# from ctypes import *

from PyQt4.Qt import Qt
from PyQt4.QtGui import (
  QMainWindow,
  QApplication,
  QWidget,
  QIcon,
  QVBoxLayout,
  QHBoxLayout,
  QLabel,
  QPushButton,
  QSizePolicy,
  QFrame,
)

PREFIX = '..'

SRC = '/imv/rawvideo/lossless_25.mkv'

PVS = [
  '/imv/h262/h262-sq.mkv',
  '/imv/h262/h262-hq.mkv',
  '/imv/h264/h264-sq.mkv',
  '/imv/h264/h264-hq.mkv',
  '/imv/h265/h265-sq.mkv',
  '/imv/h265/h265-hq.mkv',
  '/imv/vp8/vp8-sq.mkv',
  '/imv/vp8/vp8-hq.mkv',
  '/imv/vp9/vp9-sq.mkv',
  '/imv/vp9/vp9-hq.mkv',
  '/imv/theora/theora-sq.mkv',
  '/imv/theora/theora-hq.mkv',
  '/imv/dirac/dirac-sq.mkv',
  '/imv/dirac/dirac-hq.mkv',
  '/imv/prores/prores-sq.mov',
  '/imv/prores/prores-hq.mov',
]

TITLES = [
  'MPEG-2 - Qualità standard',
  'MPEG-2 - Qualità alta',
  'MPEG-4 - Qualità standard',
  'MPEG-4 - Qualità alta',
  'MPEG-5 - Qualità standard',
  'MPEG-5 - Qualità alta',
  'VP8 - Qualità standard',
  'VP8 - Qualità alta',
  'VP9 - Qualità standard',
  'VP9 - Qualità alta',
  'Theora - Qualità standard',
  'Theora - Qualità alta',
  'ProRes - Qualità standard',
  'ProRes - Qualità alta',
  'Dirac - Qualità standard',
  'Dirac - Qualità alta',
]

opts = '--osc=no --no-audio --no-border --geometry=0:0 --fullscreen'

mpv = 'mpv.com'

class Log:

  def __init__(self):
    time = dt.now().strftime('%d-%m-%Y_%H-%M-%S.log')
    logpath = path.join('log', time)
    try:
      self.file = open(logpath, 'w')
    except IOError:
      print('Non si è potuto aprire il file di log.', file=sys.stderr)
  
  def log(self, msg):
    time = dt.now().strftime('[%d-%m-%Y][%H:%M:%S]')
    try:
      self.file.write(time + ' -- ' + msg + '\n')
      self.file.flush()
    except IOError:
      print('Errore nella scrittura del log.', file=sys.stderr)
  
  def close(self):
    try:
      self.file.close()
    except:
      print('Errore nella chiusura dei log.', file=sys.stderr)

class Window(QMainWindow):

  def __init__(self, log):
    self.log = log
    self._check()
    
    # init application
    self.app = QApplication(sys.argv)
    
    super().__init__()
    
    # title, icon, ecc...
    self.setWindowTitle('Codec Evaluation')
    self.setWindowIcon(QIcon('icon.png'))
    
    # widgets and layouts
    self.setup()
    self.init()
    self.widget()
    
    # show window
    self.show()
    
    # log start
    self.log.log('evaluation started')
    
    # set fixed size
    # self.setFixedSize(self.sizeHint())
    self.resize(self.sizeHint())
  
  def _check(self):
    # check src
    if not path.isfile('{}/{}'.format(PREFIX, SRC)):
      self.log.log('{}/{} non trovato.'.format(PREFIX, SRC))
      sys.exit(255)
    for pvs in PVS:
      if not path.isfile('{}/{}'.format(PREFIX, pvs)):
        self.log.log('{}/{} non trovato.'.format(PREFIX, pvs))
        sys.exit(255)
  
  def init(self):
    cx = QWidget(self)
    self.setCentralWidget(cx)
    self.layout = QVBoxLayout()
    self.layout.setSpacing(20)
    cx.setLayout(self.layout)
  
  
  def widget(self):
    # titolo
    self.title = QLabel('Valutazione Codec Video')
    self.title.setStyleSheet('''
      QLabel {
        color: black;
        font-weight: bold;
        font-size: 28px;
      }
    ''')
    self.layout.addWidget(self.title)
    # sottotitolo
    self.subtitle = QLabel('Informatica Multimediale Video')
    self.subtitle.setStyleSheet('''
      QLabel {
        color: black;
        font-weight: bold;
        font-size: 20px;
      }
    ''')
    self.layout.addWidget(self.subtitle)
    # spiegazione
    self.desc = QLabel(
      "Quest'applicazione aiuta a valutare la qualità complessiva di un codec, "
      "confrontando visualmente una sua codifica con una sorgente non "
      "alterata che serve da controllo.\n\n"
      "Verranno mostrati diversi passaggi in cui sarà chiesto di visualizzare "
      "prima la sequenza video sorgente e successivamente quella compressa, "
      "per poterne valutare la qualità a confronto.")
    self.desc.setStyleSheet('''
      QLabel {
        color: black;
        max-width: '''+str(self.desc.sizeHint().width())+'''px;
        min-height: 200px;
      }
    ''')
    self.desc.setWordWrap(1)
    self.layout.addWidget(self.desc)
    # bottone src
    self.src = QPushButton('Riproduci sorgente')
    self.src.hide()
    self.layout.addWidget(self.src)
    # bottone pvs
    self.pvs = QPushButton('Riproduci codifica')
    self.pvs.hide()
    self.layout.addWidget(self.pvs)
    
    # add a widget with horizontal layout
    self.h_layout = QHBoxLayout()
    self.h_layout.addStretch(1)
    self.layout.addLayout(self.h_layout)
    
    # avanti
    self.next = QPushButton('Comincia')
    self.h_layout.addWidget(self.next)
    self.next.clicked.connect(self._init_eval)
  
  def _init_eval(self):
    # set widgets to eval
    self.subtitle.hide()
    self.src.show()
    self.pvs.show()
    self.i = 0
    self.eval()
  
  # def _geometry(self):
    # win = windll.user32
    # return (win.GetSystemMetrics(0), win.GetSystemMetrics(1))
  
  def process(self, command):
    ret = sp.Popen(command, shell=True,
        stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    return ret
  
  def setup(self):
    self.mpv = '{} {} '.format(mpv, opts) + '{}{}'
    self.process(mpv)
  
  def _play_src(self):
    try:
      self.src.clicked.disconnect(self._play_src)
      self.log.log('play {}/{}'.format(PREFIX, SRC))
      self.process(self.mpv.format(PREFIX, SRC))
    except TypeError:
      pass
  
  def _play_pvs(self):
    try:
      self.pvs.clicked.disconnect(self._play_pvs)
      self.log.log('play {}/{}'.format(PREFIX, PVS[self.i]))
      self.process(self.mpv.format(PREFIX, PVS[self.i]))
    except TypeError:
      pass
  
  def _increment(self, i=None):
    self.next.clicked.disconnect(self._increment)
    self.i += 1
    if self.i == len(PVS):
      self.end()
    else:
      self.eval()
  
  def eval(self):
    # title
    self.title.setText(TITLES[self.i])
    # description
    self.desc.setText(
      "È necessario guardare prima la sorgente e successivamente la codifica.\n"
      "Dopo aver guardato le sequenze, dai un giudizio sulla qualità della "
      "codifica in relazione alla sorgente, marcando sul foglio la voce che ti "
      "sembra adatta.\n\n"
      "[COMANDI]\n"
      "P per mettere in pausa il video.\n"
      "Q per chiudere il video."
    )
    self.resize(self.sizeHint())
    # src and pvs buttons
    self.src.clicked.connect(self._play_src)
    self.pvs.clicked.connect(self._play_pvs)
    # avanti
    self.next.setText('Avanti')
    self.next.clicked.connect(self._increment)
  
  def end(self):
    # title
    self.title.setText('Valutazione completata')
    # description
    self.desc.setText(
      "Hai completato la valutazione.\n"
      "Grazie per la collaborazione!"
    )
    # hide src e pvs
    self.src.hide()
    self.pvs.hide()
    # avanti -> chiudi
    self.next.setText('Chiudi')
    self.next.clicked.connect(self.close)
  
  def closeEvent(self, event):
    self.log.log('evaluation terminated')
    self.log.close()
  
  def close(self):
    sys.exit(self.app.exec_())

if __name__ == '__main__':
    log = Log()
    win = Window(log)
    win.close()
